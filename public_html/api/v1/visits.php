<?php

use DKDev\Api\ApiController;
use DKDev\App;

require_once '../../../init/init.php';

$result = (new ApiController(App::Request(), App::Logger()))->execute();
$result->send();
