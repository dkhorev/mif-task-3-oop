<?php
try {
    $dotenv = new Dotenv\Dotenv(__DIR__ . '/..');
    $dotenv->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    echo $e->getMessage();
    die;
}

if (
    !getenv('DB_HOST')
    || !getenv('DB_NAME')
    || !getenv('DB_USER')
) {
    throw new Exception('!!! Внесите настройки в файл .env !!!');
}

// конфиг бд
define('DB_HOST', getenv('DB_HOST'));
define('DB_PORT', getenv('DB_PORT'));
define('DB_NAME', getenv('DB_NAME'));
define('DB_USER', getenv('DB_USER'));
define('DB_PASS', getenv('DB_PASS'));
