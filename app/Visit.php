<?php

namespace DKDev;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

/**
 * Class Visit
 *
 * @mixin \Illuminate\Database\Eloquent\
 * @package DKDev
 *
 * @method static Builder byPeriod($from, $to)
 * @method static Builder byDay($day)
 */
class Visit extends Model
{
    protected $fillable = [
        'datetime',
        'ip',
    ];


    /**
     * @param $arVisit
     *
     * @return bool
     */
    public static function saveNew($arVisit)
    {
        return Visit::query()->insert($arVisit);
    }

    /**
     * @param $time
     *
     * @return string
     */
    public function getDatetimeAttribute($time)
    {
        return Carbon::parse($time);
    }

    /**
     * @param Builder $query
     * @param         $from
     * @param         $to
     *
     * @return Builder
     */
    public function scopeByPeriod($query, $from, $to)
    {
        $dateFrom = Carbon::parse($from);
        $dateTo = Carbon::parse($to);

        return $query->where('datetime', '>=', $dateFrom->toDateString())
                     ->where('datetime', '<', $dateTo->toDateString());
    }

    /**
     * @param Builder $query
     * @param         $day
     *
     * @return Builder
     */
    public function scopeByDay($query, $day)
    {
        $dateFrom = Carbon::parse($day);
        $dateTo = $dateFrom->copy()->addDay();

        return $query->where('datetime', '>=', $dateFrom->toDateString())
                     ->where('datetime', '<', $dateTo->toDateString());
    }
}