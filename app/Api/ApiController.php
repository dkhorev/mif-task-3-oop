<?php


namespace DKDev\Api;

use DKDev\Api\Format\FormatInterface;
use DKDev\Log\LoggerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiController
{
    /**
     * @var array
     */
    protected $request;
    /**
     * @var FormatInterface
     */
    protected $format;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    protected $query;

    public function __construct(Request $request, LoggerInterface $logger)
    {
        $this->request = $request;
        $this->query = array_merge($request->query->all(), $request->request->all());
        $this->logger = $logger;
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        // log
        $this->logger->add($this->request);

        // work
        try {
            $this->verifyInput();
            $this->format = $this->getFormatter();
            $arResult = $this->runOperation();
        } catch (Exception $e) {
            return json_encode([
                'error' => true,
                'text'  => $e->getMessage(),
            ]);
        }

        // result
        if ($arResult && $this->format) {
            return $this->format->format($arResult);
        }

        return new Response('', Response::HTTP_BAD_REQUEST);
    }

    /**
     * @throws Exception
     */
    private function verifyInput()
    {
        if (!$this->query['from'] || !$this->query['to']) {
            throw new Exception('Не указаны даты от/до');
        }

        if (!$this->query['operation']) {
            throw new Exception('Не указан тип запроса в API');
        }

        if (!$this->query['period']) {
            throw new Exception('Не указан период');
        }

        if (!$this->query['format']) {
            throw new Exception('Не указан желаемый формат ответа');
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    private function runOperation()
    {
        $class = 'DKDev\\Api\\Operation\\' . ucfirst($this->query['operation']);
        if (class_exists($class)) {
            return (new $class)->run($this->query);
        } else {
            throw new Exception('Не найден обработчик операции ' . $class);
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    private function getFormatter()
    {
        $class = 'DKDev\\Api\\Format\\' . ucfirst($this->query['format']) . 'Format';
        if (class_exists($class)) {
            return (new $class);
        } else {
            throw new Exception('Нет конвертера в формат ' . $class);
        }
    }

}
