<?php

namespace DKDev\Api\Format;

interface FormatInterface
{
    public function format($data);
}