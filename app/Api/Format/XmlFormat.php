<?php


namespace DKDev\Api\Format;

use SimpleXMLElement;
use Symfony\Component\HttpFoundation\Response;

class XmlFormat implements FormatInterface
{

    public function format($data)
    {
        $xml_data = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><Result></Result>');
        $this->array_to_xml($data, $xml_data);

        return new Response(
            $xml_data->asXML(),
            Response::HTTP_OK,
            array('content-type' => 'application/xml')
        );
    }

    /**
     * convert array to xml
     *
     * @param                   $data
     * @param SimpleXMLElement $xml_data
     */
    private function array_to_xml($data, SimpleXMLElement &$xml_data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml_data->addChild("$key");
                    $this->array_to_xml($value, $subnode);
                } else {
                    $this->array_to_xml($value, $xml_data);
                }
            } else {
                $xml_data->addChild("$key", "$value");
            }
        }
    }
}