<?php


namespace DKDev\Api\Format;

use Symfony\Component\HttpFoundation\Response;

class JsonFormat implements FormatInterface
{
    public function format($data)
    {
        return new Response(
            json_encode($data),
            Response::HTTP_OK,
            array('content-type' => 'application/json')
        );
    }
}