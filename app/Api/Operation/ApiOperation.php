<?php


namespace DKDev\Api\Operation;


interface ApiOperation
{
    public function run($request): array;
}