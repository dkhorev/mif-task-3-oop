<?php

namespace DKDev;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

/**
 * Class VisitStat
 *
 * @mixin \Illuminate\Database\Eloquent\
 * @package DKDev
 *
 * @method static Builder byPeriod($from, $to)
 * @method static Builder byDay($day)
 */
class VisitStat extends Model
{
    public $timestamps = false;

    protected $hidden = ['id', 'period'];

    protected $fillable = [
        'date',
        'period',
        'period_num',
        'visits',
    ];

    /**
     * @param $time
     *
     * @return string
     */
    public function getDateAttribute($time)
    {
        return Carbon::parse($time);
    }

    /**
     * @param Builder $query
     * @param         $from
     * @param         $to
     *
     * @return Builder
     */
    public function scopeByPeriod($query, $from, $to)
    {
        $dateFrom = Carbon::parse($from);
        $dateTo = Carbon::parse($to);

        return $query->where('date', '>=', $dateFrom->toDateString())
                     ->where('date', '<=', $dateTo->toDateString());
    }

    /**
     * @param Builder $query
     * @param         $day
     *
     * @return Builder
     */
    public function scopeByDay($query, $day)
    {
        $dateFrom = Carbon::parse($day);
        $dateTo = $dateFrom->copy()->addDay();

        return $query->where('date', '>=', $dateFrom->toDateString())
                     ->where('date', '<', $dateTo->toDateString());
    }
}