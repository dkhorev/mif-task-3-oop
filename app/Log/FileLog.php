<?php


namespace DKDev\Log;


use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Request;

class FileLog implements LoggerInterface
{
    /* @var Logger */
    protected static $log;

    public function add(Request $request)
    {
        if (!static::$log) {
            static::$log = new Logger('FileLog');
            static::$log->pushHandler(new StreamHandler(
                APP_LOGGER_FILE_PATH . date('d-m-Y') . '.log',
                Logger::INFO
            ));
        }
        // dump(static::$log);
        static::$log->info('API request:', [
            'ip'      => $request->server->get('REMOTE_ADDR'),
            'request' => $request->query->all(),
        ]);
    }
}