<?php

namespace DKDev;

use DKDev\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * контейнер приложения
 *
 * Class App
 *
 * @package DKDev
 */
class App
{
    // св-ва для всех приложений
    protected static $request;
    protected static $capsule;
    protected static $logger;

    // св-ва проекта
    // ....

    public function __construct()
    {
        if (!static::$logger) {
            static::$logger = static::initLogger();
        }

        // создаем глобальное подключение: CONNECTION_MAIN
        if (!static::$capsule) {
            $capsule = new Capsule;
            $capsule->addConnection([
                'driver'    => 'mysql',
                'host'      => DB_HOST,
                'database'  => DB_NAME,
                'username'  => DB_USER,
                'password'  => DB_PASS,
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
            ]);
            $capsule->setAsGlobal();
            $capsule->bootEloquent();
            static::$capsule = $capsule;
        }
    }

    /**
     * наш объект для работы с БД
     *
     * @return \Illuminate\Database\Capsule\Manager
     */
    public static function Capsule()
    {
        return static::$capsule;
    }

    /**
     * @return mixed
     */
    public static function Logger()
    {
        return static::$logger;
    }

    /**
     * @return Request
     */
    public static function Request()
    {
        if (!static::$request) {
            static::$request = Request::createFromGlobals();
        }

        return static::$request;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    private static function initLogger()
    {
        /* @var $class LoggerInterface */
        $class = 'DKDev\\Log\\' .ucfirst(APP_LOGGER) . 'Log';
        if (class_exists($class)) {
            return (new $class);
        } else {
            throw new \Exception('Нет обработчика логов типа ' . $class);
        }
    }
}