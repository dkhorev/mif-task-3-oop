<?php

use DKDev\App;
use DKDev\Operation\ParseVisitorStats;
use DKDev\Visit;
use Symfony\Component\HttpFoundation\JsonResponse;

require_once '../init/init.php';

$request = App::Request();
$request = array_merge($request->query->all(), $request->request->all());



if (!$request['from'] || !$request['to'] || !$request['period']) {
    die('need date and period');
}

// обработка данных по запросам
if ($request['ajax'] === 'Y' && isset($request['lastId'])) {
    $result = (new ParseVisitorStats)->make($request);
    $response = new JsonResponse($result);
    $response->send();
    die;
}

$nMaxCount = Visit::query()->count();

// if ($request['from'] === $request['to']) {
//     $nMaxCount = Visit::byDay($request['from'])->count();
// } else {
//     $nMaxCount = Visit::byPeriod($request['from'], $request['to'])->count();
// }
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Генератор статистики уникальных посетителей</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>

<body>
<div id="app" class="container">
    <div class="bs-callout bs-callout-danger">
        <h4>Генератор статистики уникальных посетителей</h4>
    </div>
    <div class="alert alert-dismissable alert-warning" style="display: none" id="error"></div>

    <span>Подождите...</span>
    <div class="progress my-2">
        <div class="progress-bar" id="status_progress" role="progressbar" style="width: 0;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
            <span>Обработка лога <span id="status_perc">0</span>%</span>
        </div>
    </div>

    <div id="result"></div>

</div>


<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

<script>
$(function () {
  let maxCount = <?= $nMaxCount ?>;
  console.log(maxCount)
  let lastId  = 0
  let total = 0
  const url_string = location.href
  const url = new URL(url_string)
  const from = url.searchParams.get('from')
  const to = url.searchParams.get('to')
  const period = url.searchParams.get('period')

  function nextStep (lastId)
  {
    $.ajax({
      url: location.pathname,
      data: {ajax: 'Y', from, to, period, lastId},
      type: 'POST',
      dataType: 'html',
      timeout: 60000,
      async: true,
      success: function (response) {
        console.log(response)
        const data = JSON.parse(response)

        total += data['count']

        // updateProgress(total)
        updateProgress(data['lastId'])

        if (data['isFinished']) {
          updateProgress(maxCount)
          $('#result').html('<a target="_blank" href="/public_html/api/v1/visits.php?operation=getVisits&from='+from+'&to='+to+'&period='+period+'&format=json">Запрос к статистике Json >></a>')
          $('#result').append('<br><a target="_blank" href="/public_html/api/v1/visits.php?operation=getVisits&from='+from+'&to='+to+'&period='+period+'&format=xml">Запрос к статистике Xml >></a>')
        } else {
          nextStep(data['lastId'])
        }
      }
    })
  }

  nextStep(lastId)

  function updateProgress (count) {
    count = parseInt(count)
    const total = parseInt(maxCount)

    const elBar = $('#status_progress')
    const elPerc = $('#status_perc')

    let percent = parseInt(count / total * 100)
    elBar.css('width', percent + '%')
    elPerc.html(percent)
  }

})
</script>

</body>
</html>
