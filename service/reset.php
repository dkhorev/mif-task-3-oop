<?php

use DKDev\App;

require_once '../init/init.php';

// Сброс БД для демо
$capsule = App::Capsule();

// destroy DB
$dbName = DB_NAME;
$capsule->getConnection()->getPdo()->exec("DROP DATABASE IF EXISTS `{$dbName}`");

echo 'OK';
