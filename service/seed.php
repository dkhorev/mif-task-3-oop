<?php

use DKDev\App;
use DKDev\Visit;

require_once '../init/init.php';

// dump(App::Capsule());
// dump(Visitor::all());

$request = App::Request()->query->all();

if ($request['from'] && $request['to'] && $request['count']) {

    $faker = Faker\Factory::create();

    $randDate = function() use ($request, $faker) {
        return $faker->dateTimeBetween($request['from'], $request['to']);
    };

    $randIp = function() use ($faker) {
        return $faker->ipv4;
    };

    $arInsert = [];
    // рандомные хиты
    for ($j = 0; $j < $request['count']; $j++) {
        $arInsert[] = [
            'datetime' => $randDate(),
            'ip'   => $randIp(),
        ];

        if (count($arInsert) > 10000) {
            Visit::query()->insert($arInsert);
            $arInsert = [];
        }
    }

    if (count($arInsert) > 0) {
        Visit::query()->insert($arInsert);
    }

    echo 'OK';
}

